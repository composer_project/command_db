<?php

namespace Steveng\CommandDb\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;

class DBCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'restructure-status-for-database';
    protected $signature = 'publish:command-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'for enhance database data for fullfill new structure of status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call("migrate");
        return print_r('done');
    }

}
